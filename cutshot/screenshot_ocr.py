import pathlib

# Import Tesseract-OCR
try:
    from PIL import Image
except ImportError:
    import Image
import pytesseract


class OCR(object):
    def __init__(self):
        self.path = pathlib.Path.cwd()
        self.path = self.path.joinpath("_CommonRedist/Tesseract-OCR/tesseract.exe")

        pytesseract.pytesseract.tesseract_cmd = r"{}".format(self.path)


    def get_str(self, img):
        return pytesseract.image_to_string(img)

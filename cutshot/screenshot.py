try:
    from PIL import Image
except ImportError:
    import Image
import mss


class Screenshot(object):

    def __init__(self, monitor, output):
        # The screen part to capture
        self.monitor = monitor
        self.output = output


    def fire(self):
        with mss.mss() as sct:
            # Grab the data
            sct_img = sct.grab(self.monitor)

            # Save to the picture file
            #mss.tools.to_png(sct_img.rgb, sct_img.size, output=self.output)

            img = Image.frombytes("RGB", sct_img.size, sct_img.bgra, "raw", "BGRX")

            return img

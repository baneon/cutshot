from pynput import keyboard, mouse


class InputController(object):

    def __init__(self, MainWindow):
        self.MainWindow = MainWindow
        self.p_ini = None
        self.p_end = None
        self.listener_kb = None

        self.ctrl_y = keyboard.HotKey.parse("<ctrl>+y")
        self.hotkey = keyboard.HotKey(self.ctrl_y, self.pulsa_ctrl_y)


    def press(self, key):
        self.hotkey.press(self.listener_kb.canonical(key))


    def release(self, key):
        self.hotkey.release(self.listener_kb.canonical(key))


    def pulsa_ctrl_y(self):
        self.MainWindow.get_translation()


    def on_click(self, x, y, button, pressed):
        if pressed:
            self.p_ini = (x, y)
        else:
            self.p_end = (x, y)
        if not pressed:
            # Stop listener
            return False


    def kb_start(self):
        # Collect events until released - Keyboard
        self.listener_kb = keyboard.Listener(
                on_press=self.press,
                on_release=self.release)
        self.listener_kb.start()


    def click_start(self):
        # Collect events until released - Mouse
        self.listener_click = mouse.Listener(on_click=self.on_click)
        self.listener_click.start()


    def click_is_alive(self):
        return self.listener_click.is_alive()

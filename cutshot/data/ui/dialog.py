# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'dialog.ui'
#
# Created by: PyQt5 UI code generator 5.14.2
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_Dialog(object):

    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(400, 300)

        self.gridLayout_2 = QtWidgets.QGridLayout(Dialog)
        self.gridLayout_2.setObjectName("gridLayout_2")

        self.gridLayout = QtWidgets.QGridLayout()
        self.gridLayout.setObjectName("gridLayout")
        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.verticalLayout.setObjectName("verticalLayout")
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")

        self.delay = QtWidgets.QLabel(Dialog)
        self.delay.setAlignment(QtCore.Qt.AlignCenter)
        self.delay.setObjectName("delay")
        self.horizontalLayout.addWidget(self.delay)

        self.spinBox = QtWidgets.QSpinBox(Dialog)
        self.spinBox.setObjectName("spinBox")
        self.horizontalLayout.addWidget(self.spinBox)

        self.verticalLayout.addLayout(self.horizontalLayout)


        self.indicator = QtWidgets.QLabel(Dialog)
        self.indicator.setStyleSheet("color:red")
        self.indicator.setAlignment(QtCore.Qt.AlignBottom|QtCore.Qt.AlignHCenter)
        self.indicator.setObjectName("indicator")
        self.verticalLayout.addWidget(self.indicator)

        self.coordinate = QtWidgets.QLabel(Dialog)
        self.coordinate.setAlignment(QtCore.Qt.AlignCenter)
        self.coordinate.setObjectName("coordinate")
        self.verticalLayout.addWidget(self.coordinate)

        self.pushStart = QtWidgets.QPushButton(Dialog)
        self.pushStart.setObjectName("pushStart")
        self.verticalLayout.addWidget(self.pushStart)

        self.gridLayout.addLayout(self.verticalLayout, 0, 0, 1, 1)
        self.gridLayout_2.addLayout(self.gridLayout, 0, 0, 1, 1)

        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)


    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog", "Dialog"))
        self.delay.setText(_translate("Dialog", "Tiempo de retardo"))
        self.indicator.setText(_translate("Dialog", "No puedes capturar la pantalla"))
        self.coordinate.setText(_translate("Dialog", "( N/A )"))
        self.pushStart.setText(_translate("Dialog", "Comenzar"))

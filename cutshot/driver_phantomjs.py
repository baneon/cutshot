import pathlib
import signal
import time

from selenium import webdriver

class UsePhantomJS(object):

    def __init__(self):
        self.path = pathlib.Path.cwd()
        self.path = self.path.joinpath("_CommonRedist/phantomjs-windows/bin/phantomjs.exe")
        self.driver = webdriver.PhantomJS(executable_path=r"{}".format(self.path))
        self.deepl = "https://www.deepl.com/translator#en/es/{}"
        self.google = "https://translate.google.com/#en/es/{}"


    def translate(self, msg):
        try:
            n = 0
            while True:
                self.driver.get(self.deepl.format(msg))
                time.sleep(n)
                elem = self.driver.find_element_by_css_selector("#target-dummydiv")
                text = elem.get_attribute("innerHTML")
                if text:
                    break
                else:
                    n += 1
            return text
        except:
            n = 0
            while True:
                self.driver.get(self.google.format(msg))
                time.sleep(n)
                elem = self.driver.find_element_by_css_selector(".JLqJ4b > span:nth-child(1)")
                text = elem.get_attribute("innerHTML")
                if text:
                    break
                else:
                    n += 1
            return text
        finally:
            # kill the specific phantomjs child proc
            self.driver.service.process.send_signal(signal.SIGTERM)
            self.driver.quit()

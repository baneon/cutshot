import sys
import time
import threading

# Third party
from PyQt5 import QtWidgets
from PyQt5.QtCore import pyqtSlot, QTimer

# screenshot
import driver_phantomjs
import inctrl
import screenshot
import screenshot_ocr

# ui
from data.ui.main import Ui_MainWindow as MainWindow
from data.ui.dialog import Ui_Dialog as Form


class UiDialog(QtWidgets.QDialog):

    def __init__(self):
        super(UiDialog, self).__init__()
        self.ui = Form()
        self.ui.setupUi(self)


class main(QtWidgets.QMainWindow):

    def __init__(self):
        super(main, self).__init__()
        self.ui = MainWindow()
        self.ui.setupUi(self)

        self.ui.pushCapture.clicked["bool"].connect(self.openDialog)

        # Var local
        self.x_min = None
        self.y_min = None
        self.width = None
        self.height = None

        # start controller keyboard
        self.controller = inctrl.InputController(self)
        self.controller.kb_start()


    def get_translation(self):
        if self.ui.plainTextEdit.document().toPlainText():
            self.ui.plainTextEdit.clear()
        if self.x_min != None and self.y_min != None:
            if self.width > 0 and self.height > 0:
                monitor = {
                    "top": self.y_min,
                    "left": self.x_min,
                    "width": self.width,
                    "height": self.height,
                }
                output = "sct-{top}x{left}_{width}x{height}.png".format(
                        **monitor)
                sct = screenshot.Screenshot(monitor, output)
                img = sct.fire()
                sct_ocr = screenshot_ocr.OCR()
                sct_str = sct_ocr.get_str(img)

                # Delete an annoying character that detects tesseract
                sct_str = sct_str.translate({ord("\x0c"): None}).strip()

                # driver is loaded
                browser = driver_phantomjs.UsePhantomJS()
                text = browser.translate(sct_str)

                self.ui.plainTextEdit.appendPlainText(text + "\n")


    def get_rect(self):
        while self.controller.click_is_alive():
            pass
        p_ini = self.controller.p_ini
        p_end = self.controller.p_end
        self.Dialog.ui.coordinate.setText("{0} , {1}".format(p_ini, p_end))
        self.Dialog.ui.indicator.setStyleSheet("color:red")
        self.Dialog.ui.indicator.setText("No puedes capturar la pantalla")

        # The minor points are obtained to coordinate the top-left points
        x_min = p_ini[0] if p_ini[0] <= p_end[0] else p_end[0]
        y_min = p_ini[1] if p_ini[1] <= p_end[1] else p_end[1]

        self.x_min = x_min
        self.y_min = y_min

        x_maj = p_ini[0] if p_ini[0] >= p_end[0] else p_end[0]
        y_maj = p_ini[1] if p_ini[1] >= p_end[1] else p_end[1]

        # Calculate the size of the height and width of the rectangle
        self.width = (x_maj) - (x_min)
        self.height = (y_maj) - (y_min)

        return False


    def start_rect(self):
        self.Dialog.ui.indicator.setStyleSheet("color:green")
        self.Dialog.ui.indicator.setText("Ya puedes capturar")
        self.controller.click_start()
        time.sleep(0.1)
        t = threading.Thread(name="get_rect", target=self.get_rect)
        t.start()


    @pyqtSlot(bool)
    def openDialog(self, _):
        self.Dialog = UiDialog()
        dly_rect = lambda delay: QTimer.singleShot(delay*1000, self.start_rect)
        self.Dialog.ui.pushStart.clicked.connect(
                lambda: dly_rect(self.Dialog.ui.spinBox.value()))
        self.Dialog.exec()


    def closeWindow(self):
        self.close()


if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    application = main()
    application.show()
    sys.exit(app.exec())
